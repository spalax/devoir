* devoir 1.1.0 (2024-12-27)

    * Add Python3.13 support.

    -- Louis Paternault <spalax@gresille.org>

* devoir 1.0.0 (2023-08-18)

    * Drop python3.4 to python3.7 support.
    * Add python3.8 to python3.12 support.
    * Add a --dry-run option.

    -- Louis Paternault <spalax@gresille.org>

* devoir 0.1.1 (2015-06-13)

    * Python3.5 support
    * Several minor improvements to setup, test and documentation.

    -- Louis Paternault <spalax@gresille.org>

* devoir 0.1.0 (2015-03-20)

    * First published version.

    -- Louis Paternault <spalax@gresille.org>
